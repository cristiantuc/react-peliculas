import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route  } from "react-router-dom";
import Home from './components/Home';
import MoviePage from './components/MoviePage';

function App() {
  const API_KEY = '1e8415b0cd8cc3cbdf72cf9bc3004db6';
  const [generos, setGeneros] = useState([]);
  const [generoSelec, setGeneroSelec] = useState(-1);
  const [peliculas, setPeliculas] = useState([]);

  useEffect( () => {
    async function fetchGeneros() {
      const solicitud = await fetch(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=es-ES`);
      const respuesta = await solicitud.json();
      setGeneros(respuesta["genres"]);
    }
    fetchGeneros();

    if (generoSelec > 0) {
      async function fetchPorGenero() {
        const solicitud = await fetch(
          `https://api.themoviedb.org/3/discover/movie/?api_key=${API_KEY}&with_genres=${generoSelec}&language=es-ES&page=1`);
        const respuesta = await solicitud.json();
        setPeliculas(respuesta["results"]);
      }
      fetchPorGenero();
    } else {
      async function fetchEnCartel() {
        const solicitud = await fetch(
          `https://api.themoviedb.org/3/movie/now_playing?api_key=${API_KEY}&language=es-ES&page=1`);
        const respuesta = await solicitud.json();
        setPeliculas(respuesta["results"]);
      }
      fetchEnCartel();
    }
  }, [generoSelec]);

  const guardarGenSelec = (e) => {
    setGeneroSelec(e.target.value);
  }

  return (
    <div>
      <Router>
        <Switch>
          <Route path='/' exact>
            <Home 
              generos={generos} 
              onClick={guardarGenSelec}
              generoSelec={generoSelec}
              peliculas={peliculas}
            />
          </Route>
          <Route path='/movie' exact>
            <MoviePage/>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;