import React from 'react';
import { Link } from 'react-router-dom';

function MovieHeader() {
  return(
    <div className="d-flex aling-items-end m-0 bg-dark">
      <nav className="navbar navbar-expand-lg navbar-dark bg-drak">
        <Link to='/'>
          <h4 className="navbar-brand text-light">
            <svg className="bi bi-arrow-left-short" width="2em" height="2em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path  d="M7.854 4.646a.5.5 0 010 .708L5.207 8l2.647 2.646a.5.5 0 01-.708.708l-3-3a.5.5 0 010-.708l3-3a.5.5 0 01.708 0z"/>
              <path  d="M4.5 8a.5.5 0 01.5-.5h6.5a.5.5 0 010 1H5a.5.5 0 01-.5-.5z" />
            </svg>
            Galería
          </h4>
        </Link>
      </nav>
    </div>
  );
}

export default MovieHeader