import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router';
import './MovieContent.css';

function MovieContent() {
  const API_KEY = '1e8415b0cd8cc3cbdf72cf9bc3004db6';
  let location = useLocation();
  const [detallesPeli, setDetallesPeli] = useState([]);
  
  useEffect( () => {
    async function fetchMovie() {
      const solicitud = await fetch(
        `https://api.themoviedb.org/3/movie/${location["state"]["idMovie"]}?api_key=${API_KEY}&language=es-ES`);
      const respuesta = await solicitud.json();
      setDetallesPeli(respuesta);
    }
    fetchMovie();
  }, [location]);

  if (detallesPeli !== [] || detallesPeli !== null) {
    return(
      <div id="moviecontent">
        <div className="card bg-dark text-white">
          <img src={`https://image.tmdb.org/t/p/w780${detallesPeli["backdrop_path"]}`} 
            className="card-img border border-0 rounded-0" 
            alt="..."
          />
          <div className="card-img-overlay p-0">
            <div className="card-body bg-dark bg-opacity">
              <h5 className="card-title">{detallesPeli["title"]}</h5>
              <p className="card-text">{detallesPeli["overview"]}</p>
              <a 
                className="card-text" 
                href={detallesPeli["homepage"]}
                target="_blank"
                rel="noopener noreferrer"
              >
                Ir a página de la película 
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return <h3>Cargando...</h3>
  }
}

export default MovieContent;