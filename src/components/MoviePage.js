import React from 'react';
import MovieHeader from './MovieHeader';
import MovieContent from './MovieContent';

function MoviePage() {
  return(
    <div>
      <MovieHeader/>
      <MovieContent/>
    </div>
  );
}

export default MoviePage;