import React from 'react';
import './Galery.css'
import { Link } from 'react-router-dom';

function Galery({peliculas, listaGeneros, generoSelec}) {
  let etiqueta;
  
  if (generoSelec > 0) {
    etiqueta = listaGeneros.find(seleccion => seleccion["id"] === parseInt(generoSelec))["name"];
  } else {
    etiqueta = "En cartelera";
  }

  return(
    <div id="galery">
      <h4 className="mt-3 ml-3 pt-3 pl-4">{etiqueta}</h4>
      <div className="d-flex flex-row flex-wrap justify-content-start p-0 m-4">
      {
        peliculas.map((pelicula, index) => {
          return(
            <Link 
              to={{
                pathname: '/movie',
                state: {
                  idMovie: pelicula["id"]
                }
              }}
              className="row m-0 px-3 w-25" 
              key={index } 
              value={pelicula["id"]}
              >
              <div className="row h-100 px-2">
                <div className="card bg-color-card p-1 my-3 rounded-0">
                  <img 
                    src={`https://image.tmdb.org/t/p/w500${pelicula["poster_path"]}`} 
                    className="card-img-top" alt="..."
                  />
                  <div className="card-body text-white bg-dark">
                    <h5 className="card-title">{pelicula["title"]}</h5>
                    <p className="card-text">
                      <small className="text-muted">
                          <b>{pelicula["release_date"]}</b>
                      </small>
                    </p>
                  </div>
                </div>
              </div>
            </Link>
          )
        })
      }
      </div>
    </div>
  );
}

export default Galery;