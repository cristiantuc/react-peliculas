import React from 'react';
import './GaleryHeader.css';

function GaleryHeader({listaGeneros, onClick}) {

  return (
    <div className="d-flex aling-items-end m-0 bg-dark">
      <nav className="navbar navbar-expand-lg navbar-dark bg-drak">
        <h4 className="navbar-brand text-light">React Películas</h4>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item dropdown">
              <h6
                className="btn nav-link dropdown-toggle text-light" 
                id="navbarDropdown" 
                role="button" 
                data-toggle="dropdown" 
                aria-haspopup="true" 
                aria-expanded="false"
              >
                Géneros
              </h6>
              <div 
                className="dropdown-menu bg-dark" 
                aria-labelledby="navbarDropdown"
              >
                {
                  listaGeneros.map((genero, index) => {
                    return(
                      <button 
                        className="dropdown-item btn-sm" 
                        key={index} 
                        value={genero["id"]}
                        onClick={onClick}
                      >
                        {genero["name"]}
                      </button>
                    );
                  })
                }
              </div>
            </li>
          </ul>
        </div>
      </nav>
    </div>
  );
}

export default GaleryHeader;