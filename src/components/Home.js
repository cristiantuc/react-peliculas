import React from "react";
import GaleryHeader from './GaleryHeader';
import Galery from './Galery';

function Home({generos, onClick, generoSelec, peliculas}) {
  
  return(
    <div>
      <GaleryHeader 
        listaGeneros={generos} 
        onClick={onClick}
      />
      <Galery 
        listaGeneros={generos}
        generoSelec={generoSelec}
        peliculas={peliculas}
      />
    </div>
  );
}

export default Home;